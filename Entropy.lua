--[[    ENTROPY COORD   ]]-------------------------------------------------------

EntropyCoord = {
    entropy = 0,
    x = 0,
    y = 0
}

function EntropyCoord:new (x, y, entropy, o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.entropy = entropy
    o.x = x
    o.y = y
    return o 
end

-- For comparing EntropyCoords
function compare(a, b)
    if (a and b) then
        return (a.entropy < b.entropy)
    end
end