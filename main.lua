-- local heap = require "binaryheap"
local Core_Cell = require "CoreCell"
local Entropy_Coord = require "Entropy"
local Core_State = require "CoreState"
local Pre_Process = require "PreProcess"

local tile_index_grid = {}
local tile_dictionary = {}

local out_w, out_h  = 200,20
local tile_size     = 4
local scale         = 4

local is_blank = false

settings = {

     has_border =   false

    ,hor_mirror =   true
    ,ver_mirror =   true
    ,rot90 =        true
    ,rot180 =       true
    ,rot270 =       true

}

grid = {}


--[[    If the algorithm isn't generating an image from scratch, 
    it needs to adapt to the existing restraints. That's done by
    updating the CoreCells possible[] and entropy values through
    remove_tile().
    ]]
function initialise (out_x, out_y, reference)
    
    local r,g,b,a
    local cell

    grid.x = out_x
    grid.y = out_y
    for i=0, grid.y-1 do
        for j=0, grid.x-1 do
            
            r,g,b,a = reference:getPixel(j, i)
            cell = grid[i][j]
            
            if (a > 0.5) then
                r = math.floor(r*255)
                g = math.floor(g*255)
                b = math.floor(b*255)
                local color = string.format("%03d%03d%03d", r,g,b)
                color = tonumber(color)

                cell.is_collapsed = true
                core_state.remaining_cells = core_state.remaining_cells - 1
                for t, valueT in pairs(tile_dictionary) do
                    if (valueT[0][0] ~= color) and cell.possible[t] then
                        cell.possible[t] = false
                        grid[i][j]:remove_tile(t)
                        local r_update = RemovalUpdate:new(t, j, i)
                        core_state.tile_removals:push(r_update)

                        -- This probably changed the cell's entropy.
                        local entropy_update = {}
                        local new_etropy = grid[i][j]:entropy()
                        entropy_update = EntropyCoord:new(j, i, new_etropy)
                        core_state.entropy_heap:insert(entropy_update)
                        
                    end
                end
            end
        end
    end
end

--[[	The algorithm populates a grid with tile indices in a way
	which completely respects adjacency rules, and 
	probabilistically respects frequency rules. Every pair of
	adjacent tiles will be explicitly allowed by the adjacency
	rules, and the relative frequencies of tiles in the output 
	will usually be about the same as in the frequency rules.
	]]
function wfc_core (out_x, out_y, reference)

    local num_tiles = dict_size

    grid.x = out_x
    grid.y = out_y
    for i=0,out_y-1 do
        grid[i] = {}
        for j=0,out_x-1 do
            grid[i][j] = CoreCell:new()
            --print(i, j, grid[i][j].entropy_noise)
        end
    end

    core_state = CoreState:new()    

    if not is_blank then
        initialise(out_x, out_y, reference)
    end

    core_state:run()

    tile_index_grid.x = out_x
    tile_index_grid.y = out_y
    for i=0,out_y-1 do
        tile_index_grid[i] = {}
        for j=0,out_x-1 do
            tile_index_grid[i][j] = grid[i][j]:find_the_one()
            --print(tile_index_grid[i][j], "at", i,j)
        end
    end
end


function wfc_postprocess (out_w, out_h)

    love.graphics.scale(scale,scale)

    for x=0, out_w-1 do
        for y=0, out_h-1 do
            local R,G,B = find_RGB(tile_index_grid[y][x])
            love.graphics.setColor(R,G,B)
            love.graphics.rectangle("fill",x,y,1,1)
        end
    end
end

--[[
function wfc_image (tile_size, out_x, out_y)

	input_image = love.image.newImageData("input/01.png")

	adj_rules, freq_rules, top_left_pix = wfc_preprocess(input_image, tile_size)
	tile_index_grid = wfc_core(adj_rules, freq_rules, out_x, out_y)
	return wfc_postprocess(tile_index_grid, top_left_pix)

end
]]

function love.load ()

    local input_w, input_h = 240,200
    local reference
    math.randomseed(os.time())

    if not is_blank then 
        reference = love.image.newImageData("input/reference2.png")
        input_w, input_h = reference:getDimensions()
        out_w, out_h = reference:getDimensions()
        print(out_w, out_h)
    end

    love.window.setMode(scale*input_w, scale*input_h,{borderless = false})
    input_image = love.image.newImageData("input/input_01.png")

--  wfc_preprocess(input_image, tile_size, hor_mirror, ver_mirror, rot90, rot180, rot270)
    wfc_preprocess(input_image, tile_size, settings)
    print ("PreProcess complete")


    local adj_rules = fetch_adj_rules()
    local freq_rules = fetch_freq_rules()
    tile_dictionary = fetch_dictionary()
    --preprocess_test(adj_rules)

    wfc_core(out_w, out_h, reference)


end

function love.draw ()
    wfc_postprocess(out_w, out_h)
end