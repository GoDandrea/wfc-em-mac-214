--[[    STACK       ]]-----------------------------------------------------------

Stack = {}

function Stack:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function Stack:push(item)
    --print("Push", item.x, item.y, item.tile_index)
    table.insert(self, item)
end

function Stack:pop()
    aux = table.remove(self)
    --if aux then print("POP ", aux.x, aux.y, aux.tile_index)
    --else print("POP ", "nil") end
    return aux
end