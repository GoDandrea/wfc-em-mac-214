
-- adj_rules AKA Adjacency Rules is a 3D bool array
-- adj_rules[i][d][j] = can there be a tile j in direction d from a tile i?
-- direction (UP,DOWN,LEFT,RIGHT) == (1,2,3,4)
adj_rules = {}
function fetch_adj_rules()
    return adj_rules
end

-- freq_rules AKA Frequency Rules/Hints is a regular 1D array/table
-- freq_rules[i] = n means the tile i has relative frequency n 
freq_rules = {}
function fetch_freq_rules()
    return freq_rules
end

-- tile_dictionary[t][i][j] is the [i][j] pixel of the tile t
tile_dictionary = {}
function fetch_dictionary()
    return tile_dictionary
end

-- border_dictionary[t] tells if tile t belongs to a border or not
border_dictionary = {}
function fetch_border_dictionary()
    return border_dictionary
end

-- number of tiles in the dictionary
dict_size = 0
function fetch_dict_size()
    return dict_size
end

-- OBS:
-- a TILE is a NxN matrix of colors/values, with N == tile_size
-- TILE[i][j] == color in the (j, i) pixel of TILE

local hor_mirror
local ver_mirror
local rot90
local rot180
local rot270

-- Increments the frquency count for the tile tile_id
function freq_increment (tile_id)
    if not freq_rules[tile_id] then freq_rules[tile_id] = 0 end
    freq_rules[tile_id] = freq_rules[tile_id] + 1
end


--[[ Checks if new_tile already exists in the tile_dictionary,
    and adds it to the dictionary if it doesn't.
    Returns its id, either pre-existing or new. ]]
function tile_check(new_tile, tile_size, tile_dictionary)

    local id = ""
    for i=0, tile_size-1 do
        for j=0, tile_size-1 do
            id = id .. tostring(new_tile[i][j])
        end
    end

    if not tile_dictionary[id] then
        tile_dictionary[id] = {}
        for i=0, tile_size-1 do
            tile_dictionary[id][i] = {}
            for j=0, tile_size-1 do
                tile_dictionary[id][i][j] = new_tile[i][j]
            end
        end
        dict_size = dict_size + 1
    end

    --border_dictionary[id] = is_border

    return id

end


--[[ Applies tile_check() to all rotations and mirrors of new_tile ]]
function add_to_dictionary (tile_size, new_tile) 

    local id
    local transform = {}

    -- No rotation
        -- No mirroring
        for i=0, tile_size-1 do
            transform[i] = {}
            for j=0, tile_size-1 do
                transform[i][j] = new_tile[i][j]
            end
        end
        id = tile_check(transform, tile_size, tile_dictionary)
        freq_increment(id)

        -- Horizontal mirroring
        if hor_mirror then
            for i=0, tile_size-1 do
                transform[i] = {}
                for j=0, tile_size-1 do
                    transform[i][tile_size-j-1] = new_tile[i][j]
                end
            end
            id = tile_check(transform, tile_size, tile_dictionary)
            freq_increment(id)
        end

        -- Vertical mirroring
        if ver_mirror then
            for i=0, tile_size-1 do
                transform[tile_size-i-1] = {}
                for j=0, tile_size-1 do
                    transform[tile_size-i-1][j] = new_tile[i][j]
                end
            end
            id = tile_check(transform, tile_size, tile_dictionary)
            freq_increment(id)
        end
    --

    -- 90 degrees anti-clockwise rotation
        if rot90 then
            -- No mirroring
            for i=0, tile_size-1 do
                transform[i] = {}
                for j=0, tile_size-1 do
                    transform[i][j] = new_tile[j][tile_size-i-1]
                end
            end
            id = tile_check(transform, tile_size, tile_dictionary)
            freq_increment(id)

            -- Horizontal mirroring
            if hor_mirror then
                for i=0, tile_size-1 do
                    transform[i] = {}
                    for j=0, tile_size-1 do
                        transform[i][tile_size-j-1] = new_tile[j][tile_size-i-1]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end

            -- Vertical mirroring
            if ver_mirror then
                for i=0, tile_size-1 do
                    transform[tile_size-i-1] = {}
                    for j=0, tile_size-1 do
                        transform[tile_size-i-1][j] = new_tile[j][tile_size-i-1]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end
        end
    --

    -- 180 degrees anti-clockwise rotation
        if rot180 then
        -- No mirroring
            for i=0, tile_size-1 do
                transform[i] = {}
                for j=0, tile_size-1 do
                    transform[i][j] = new_tile[tile_size-i-1][tile_size-j-1]
                end
            end
            id = tile_check(transform, tile_size, tile_dictionary)
            freq_increment(id)

            -- Horizontal mirroring
            if hor_mirror then
                for i=0, tile_size-1 do
                    transform[i] = {}
                    for j=0, tile_size-1 do
                        transform[i][tile_size-j-1] = new_tile[tile_size-i-1][tile_size-j-1]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end

            -- Vertical mirroring
            if ver_mirror then
                for i=0, tile_size-1 do
                    transform[tile_size-i-1] = {}
                    for j=0, tile_size-1 do
                        transform[tile_size-i-1][j] = new_tile[tile_size-i-1][tile_size-j-1]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end
        end
    --


    -- 270 degrees anti-clockwise rotation
        if rot270 then
            -- No mirroring
            for i=0, tile_size-1 do
                transform[i] = {}
                for j=0, tile_size-1 do
                    transform[i][j] = new_tile[tile_size-j-1][i]
                end
            end
            id = tile_check(transform, tile_size, tile_dictionary)
            freq_increment(id)

            -- Horizontal mirroring
            if hor_mirror then
                for i=0, tile_size-1 do
                    transform[i] = {}
                    for j=0, tile_size-1 do
                        transform[i][tile_size-j-1] = new_tile[tile_size-j-1][i]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end

            -- Vertical mirroring
            if ver_mirror then
                for i=0, tile_size-1 do
                    transform[tile_size-i-1] = {}
                    for j=0, tile_size-1 do
                        transform[tile_size-i-1][j] = new_tile[tile_size-j-1][i]
                    end
                end
                id = tile_check(transform, tile_size, tile_dictionary)
                freq_increment(id)
            end
        end
    --

end


--[[ Populates adj_rules with adjacency rules. ]]
function adjacency_checker (tile_dictionary, tile_size)

    --[[ compatible{} is a table of functions that check if tiles a and b 
        can be adjacent to each other in the specified direction. They are 
        superpositioned, but b is shifted one pixel/unit in that direction. ]]
    local compatible = {
        UP = function(a, b, tile_size)
            for i=0, tile_size-2 do
                for j=0, tile_size-1 do
                    if a[i][j] ~= b[i+1][j] then
                        return false
                    end
                end
            end
            return true
        end,
        DOWN = function(a, b, tile_size)
            for i=0, tile_size-2 do
                for j=0, tile_size-1 do
                    if a[i+1][j] ~= b[i][j] then
                        return false
                    end
                end
            end
            return true
        end,
        LEFT = function(a, b, tile_size)
            for i=0, tile_size-1 do
                for j=0, tile_size-2 do
                    if a[i][j] ~= b[i][j+1] then
                        return false
                    end
                end
            end
            return true
        end,
        RIGHT = function(a, b, tile_size)
            for i=0, tile_size-1 do
                for j=0, tile_size-2 do
                    if a[i][j+1] ~= b[i][j] then
                        return false
                    end
                end
            end
            return true
        end
    }

    for i, tileI in pairs(tile_dictionary) do
        adj_rules[i] = {}
        for d=1,4 do
            adj_rules[i][d] = {}
        end
        for j, tileJ in pairs(tile_dictionary) do
            adj_rules[i][1][j] = compatible.UP(tileI, tileJ, tile_size)
            adj_rules[i][2][j] = compatible.DOWN(tileI, tileJ, tile_size)
            adj_rules[i][3][j] = compatible.LEFT(tileI, tileJ, tile_size)
            adj_rules[i][4][j] = compatible.RIGHT(tileI, tileJ, tile_size)
        end
    end

end


--[[ Makes a 2D array of tile_size (AKA tile) from image, starting from the top-left corner
    of the tile ]]
function make_tile (tile_size, image, coord)

    local iW = image.width
    local iH = image.height
    new_tile = {}

    for i=0,tile_size-1 do
        new_tile[i] = {}
        for j=0, tile_size-1 do
            new_tile[i][j] = image[(coord.y+i) % iH][(coord.x+j) % iW]
        end
    end
    return new_tile

end


--[[    Finds the RGB values of a tile's top-left pixel from
    that tile's id. ]]
function find_RGB(tile_id)
    
    --print ("find_RGB:", tile_id)
    local aux = tile_dictionary[tile_id][0][0]

    local R = aux/1000000
    local G = (aux%1000000)/1000
    local B = aux%1000

    return R/255,G/255,B/255

end

function preprocess_test (adj_rules)

    for i, tileI in pairs(tile_dictionary) do
        print (i)
        print ("Neighbors:")
        for d=1,4 do
            print ("direction", d)
            for j,tileJ in pairs(tile_dictionary) do
                if adj_rules[i][d][j] then
                    print(j)
                end
            end
        end
        print (" ")
    end
end

--[[    The preprocessing of the image is responsible for populating
    the tile_dictionary with the tiles one can find in the original
    image, and also the adj_rules and freq_rules, which tells which
    tiles can be next to each other and how often each tile appears in
    the original image, respectively.
    It also makes the aux_image, a 2D array of strings easier to manipulate
    than the pixels of the original image.
    ]]
function wfc_preprocess (input_image, tile_size, settings)

    hor_mirror = settings[hor_mirror]
    ver_mirror = settings[ver_mirror]
    rot90 = settings[rot90]
    rot180 = settings[rot180]
    rot270 = settings[rot270]

    local iW = input_image:getWidth()
    local iH = input_image:getHeight()
    local aux_image = {}
    local tile = {}
    local coord = {}

    for i=0,iH-1 do
        aux_image[i] = {}
        for j=0,iW-1 do

            local r,g,b = input_image:getPixel(j,i)
            r = math.floor(r*255)
            g = math.floor(g*255)
            b = math.floor(b*255)
            local color = string.format("%03d%03d%03d",r,g,b)
            color = tonumber(color)
            aux_image[i][j] = color
        end
    end
    aux_image.width = iW
    aux_image.height = iH

    for i=0,iH-1 do
        for j=0,iW-1 do
            is_border = false
            coord.y = i 
            coord.x = j 
            tile = make_tile(tile_size, aux_image, coord)
            add_to_dictionary(tile_size, tile)
        end
    end

    adjacency_checker(tile_dictionary, tile_size)

end
